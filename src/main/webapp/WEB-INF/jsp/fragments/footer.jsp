<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<table class="footer">
    <tr>
        <td width="50%"></td>
        <td width="20%"><a href="<spring:url value="/logout.html" />" > Logout</a> </td>
        <td align="right"><img src="<spring:url value="/resources/images/spring-pivotal-logo.png" htmlEscape="true" />"
                               alt="Sponsored by Pivotal"/></td>
    </tr>
</table>


